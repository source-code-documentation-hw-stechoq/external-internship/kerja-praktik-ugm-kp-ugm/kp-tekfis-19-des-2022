//definisi pin arduino//
#define IN_PIN A2   //pin Sensor
#define OUT_PIN 5   //pin Motor

//Kebutuhan PID//
#include <PID_v1.h>
double Setpoint;   // will be the desired value
double Input;       // pressure sensor
double Output ;     // pump speed
//PID parameters
double Kp=3, Ki=8, Kd=3; 
 
//create PID instance 
PID myPID(&Input, &Output, &Setpoint, Kp, Ki, Kd, DIRECT);

//kebutuhan filter sensor//
#define WINDOW_SIZE 20    //Diperbesar jika noise masih banyak, tetapi akan semakin tidak akurat bila terlalu besar
int INDEX = 0;
int VALUE = 0;
int SUM = 0;
int READINGS[WINDOW_SIZE];

//Variabel penentu kondisi//
int flag = 0;

//Fungsi untuk tampilan awal di serial//
void pertanyaan() {
  Serial.println("Tekanan yang diinginkan?");
}

//Variabel penerima parsingan//
char a;

void setup() {
  Serial.begin(9600);   //Memulai Serial dengan baudrate sekian

  pinMode(IN_PIN, INPUT);     //Sensor
  pinMode(OUT_PIN,OUTPUT);    //Motor

//Memulai PID//
  //Turn the PID on
  myPID.SetMode(AUTOMATIC);
  //Adjust PID values
  myPID.SetTunings(Kp, Ki, Kd);

//Memastikan motor mati
  analogWrite(OUT_PIN,0);

//Tampilan awal pada serial
  pertanyaan();

}

void loop() {

if(Serial.available()>0)
  { 
      a = Serial.read();
      //Serial.println(double(a)-48);

      if (int(a)>50)
       {Setpoint = double(a) - 48;
        flag = 1;
        Serial.println(Setpoint);}

      if (a == 'x')
      { 
        analogWrite(OUT_PIN,0);
        flag = 0;
        Serial.println("Sistem Berhenti");
        pertanyaan();
      }
      
      
  }

 if (flag == 1)
  {Serial.println("Sedang bekerja..."); flag = 2; delay(2000);}

  if (flag ==  2){
    SUM = SUM - READINGS[INDEX];       // Remove the oldest entry from the sum
    VALUE = analogRead(IN_PIN);        // Read the next sensor value
    READINGS[INDEX] = VALUE;           // Add the newest reading to the window
    SUM = SUM + VALUE;                 // Add the newest reading to the sum
    INDEX = (INDEX+1) % WINDOW_SIZE;   // Increment the index, and wrap to 0 if it exceeds the window size
      
    double Input1 = double(SUM / WINDOW_SIZE);      // Divide the sum of the window by the window size for the result

    Input = (Input1 * 8)/1000; //-1*( (5 * (pow(10,-11)) * (pow(Input1,5)) )-(2* (pow(10,-7)) * (pow(Input1,4)))+(0.0003 * (pow(Input1,3)))-(0.2128 * pow(Input1,2))+(77.738 * Input1)-(11248)) ;//(Input1 * -6)/1000;

    //(p)id
    myPID.Compute();

    //Keluaran motor pump
    analogWrite(OUT_PIN,Output);

    //Menampilkan actual pressure di lcd
    Serial.println(Input);

    delay(100);
    }

 

    }
